﻿namespace DependencyExample;

class Backery
{
    public string MakeCake()
    {
        var flour = new Flour();
        var sugar = new Sugar();
        var egg = new Egg();
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        return $"Trộn {flour.Material}, {sugar.Material} & {egg.Material} để làm bánh.";
    }
}

public class Flour
{
    public string Material => "Bột mì";
}

public class Sugar
{
    public string Material => "Đường cát";
}

public class Egg
{
    public string Material => "Trứng gà";
}
