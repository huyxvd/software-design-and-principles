﻿// khởi tạo dependency
var flour = new Flour();
var sugar = new Sugar();
var egg = new Egg();

var backery = new Backery(flour, sugar, egg);
var mix = backery.MakeCake();
Console.WriteLine(mix);

// sau khi đã refactor về dependency injection
class Backery
{
    Flour flour;
    Sugar sugar;
    Egg egg;
    public Backery(Flour _flour, Sugar _sugar, Egg _egg) // dependency injection(constructor injection)
    {
        flour = _flour;
        sugar = _sugar;
        egg = _egg;
    }
    public string MakeCake()
    {
        // thay vì khởi tạo dependency ở bên trong thì truyền nó từ bên ngoài vào
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        return $"Trộn {flour.Material}, {sugar.Material} & {egg.Material} để làm bánh.";
    }
}

public class Flour
{
    public string Material => "Bột mì";
}

public class Sugar
{
    public string Material => "Đường cát";
}

public class Egg
{
    public string Material => "Trứng gà";
}
