# Dependency Injection (DI)

Dependency Injection (DI) là một nguyên tắc trong phát triển phần mềm cho phép chúng ta cung cấp các phụ thuộc (dependencies) cho một đối tượng từ bên ngoài thay vì đối tượng tự tạo hoặc quản lý phụ thuộc của mình. Điều này giúp tăng tính mô-đun và khả năng tái sử dụng trong mã nguồn.

## Lợi ích của Dependency Injection

- Reduced dependency coupling: Dependency Injection giúp chuyển trách nhiệm cung cấp phụ thuộc cho một bên thứ ba.
- Dễ dàng kiểm thử và tái sử dụng: Với DI, chúng ta có thể dễ dàng thay thế các phụ thuộc bằng các đối tượng giả (mock objects) hoặc đối tượng giả lập (dummy objects) để kiểm thử và tái sử dụng mã nguồn dễ dàng hơn.
- Khả năng mở rộng và linh hoạt: DI giúp mở rộng và thay đổi mã nguồn dễ dàng hơn, bằng cách cung cấp các phụ thuộc mới mà không cần sửa đổi mã nguồn hiện có.

## Triển khai Dependency Injection

Có nhiều cách để triển khai Dependency Injection, bao gồm:

1. Constructor Injection: Đối tượng phụ thuộc được truyền vào thông qua constructor của đối tượng sử dụng nó.
2. Setter Injection: Đối tượng phụ thuộc được truyền vào thông qua phương thức setter của đối tượng sử dụng nó.
3. Interface Injection: Đối tượng sử dụng DI được yêu cầu triển khai một interface và nhận phụ thuộc thông qua một phương thức trong interface đó.

Đối với mỗi trường hợp cụ thể, chúng ta có thể chọn phương thức triển khai phù hợp với yêu cầu và thiết kế của ứng dụng.
