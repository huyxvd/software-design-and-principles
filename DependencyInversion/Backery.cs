﻿public class Backery
{
    private readonly IFlourMaterial flour;
    private readonly ISugarMaterial sugar;
    private readonly IEggMaterial egg;
    public Backery(IFlourMaterial _flour, ISugarMaterial _sugar, IEggMaterial _egg) // dependency injection(constructor injection)
    {
        flour = _flour;
        sugar = _sugar;
        egg = _egg;
    }

    public string MakeCake()
    {
        // thay vì khởi tạo dependency ở bên trong thì truyền nó từ bên ngoài vào
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        return $"Trộn {flour.Material()}, {sugar.Material()} & {egg.Material()} để làm bánh.";
    }
}


