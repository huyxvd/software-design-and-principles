﻿public interface IEggMaterial
{
    string Material();
}

public class Egg : IEggMaterial
{
    public string Material() => "Trứng gà";
}
