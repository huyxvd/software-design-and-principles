﻿public interface IFlourMaterial
{
    string Material();
}
public class Flour : IFlourMaterial
{
    public string Material() => "Bột mì";
}

public class RiceFlour : IFlourMaterial
{
    public string Material() => "Bột gạo";
}