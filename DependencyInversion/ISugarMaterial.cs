﻿public interface ISugarMaterial
{
    string Material();
}
public class Sugar : ISugarMaterial
{
    public string Material() => "Đường cát";
}
