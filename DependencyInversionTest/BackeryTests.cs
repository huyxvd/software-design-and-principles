﻿using Moq;

namespace DependencyInversionTest
{
    public class BackeryTests
    {
        [Fact]
        public void MakeCake_ShouldMixIngredients()
        {
            // Arrange
            var mockFlour = new Mock<IFlourMaterial>();
            var mockSugar = new Mock<ISugarMaterial>();
            var mockEgg = new Mock<IEggMaterial>();

            mockFlour.Setup(f => f.Material()).Returns("bột mock");
            mockSugar.Setup(s => s.Material()).Returns("đường mock");
            mockEgg.Setup(e => e.Material()).Returns("trứng mock");

            var expected = "Trộn bột mock, đường mock & trứng mock để làm bánh.";

            var backery = new Backery(mockFlour.Object, mockSugar.Object, mockEgg.Object);

            // Act
            var actual = backery.MakeCake();

            // Assert
            mockFlour.Verify(f => f.Material(), Times.Once());
            mockSugar.Verify(s => s.Material(), Times.Once());
            mockEgg.Verify(e => e.Material(), Times.Once());
            Assert.Equal(expected, actual);
        }
    }
}