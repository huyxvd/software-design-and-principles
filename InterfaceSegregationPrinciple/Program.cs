﻿// khả năng gửi tiền vào tài khoản.
public interface IGuiTien
{
    void GuiTien(double amount);
}

// khả năng rút tiền từ tài khoản.
public interface IRutTien
{
    void RutTien(double amount);
}

public class TaiKhoanTietKiem : IGuiTien
{
    public void GuiTien(double amount)
    {
        // gửi tiền vào tài khoản.
    }
}

public class TaiKhoanRutTien : IRutTien
{
    public void RutTien(double amount)
    {
        // rút tiền từ tài khoản
    }
}

public class TaiKhoanChinh : IGuiTien, IRutTien
{
    public void GuiTien(double amount)
    {
        // gửi tiền vào tài khoản.
    }

    public void RutTien(double amount)
    {
        // rút tiền từ tài khoản
    }
}
