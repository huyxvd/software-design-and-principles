﻿Rectangle rectangle = new Rectangle();
rectangle.SetWidth(5);
rectangle.SetHeight(10);
Console.WriteLine(rectangle.GetArea()); // Kết quả: 50

Rectangle square = new Square();
square.SetWidth(5);
square.SetHeight(10); // Hành vi không đúng, vì chiều rộng và chiều cao của hình vuông phải bằng nhau
Console.WriteLine(square.GetArea()); // Kết quả: 25, không như kết quả mong muốn 50
/*

    Trong ví dụ trên, chúng ta có hai lớp, Rectangle và Square.
    Lớp Square kế thừa từ Rectangle. 
    Tuy nhiên, khi gọi các phương thức SetWidth và SetHeight trên đối tượng Square,
    hành vi không đúng đã xảy ra, vì khi set chiều rộng và chiều cao cho hình vuông,
    chúng phải bằng nhau.

    Điều này vi phạm nguyên tắc Liskov Substitution
    vì đối tượng của lớp con Square không thể thay thế hoàn toàn đối tượng của lớp cơ sở Rectangle,
    gây ra sự không đúng đắn trong hành vi.
 */

// Lớp cơ sở Rectangle
public class Rectangle
{
    protected int width;
    protected int height;

    public virtual void SetWidth(int width)
    {
        this.width = width;
    }

    public virtual void SetHeight(int height)
    {
        this.height = height;
    }

    public int GetArea()
    {
        return width * height;
    }
}

// Lớp con Square kế thừa từ Rectangle
public class Square : Rectangle
{
    public override void SetWidth(int width)
    {
        base.SetWidth(width);
        base.SetHeight(width);
    }

    public override void SetHeight(int height)
    {
        base.SetHeight(height);
        base.SetWidth(height);
    }
}