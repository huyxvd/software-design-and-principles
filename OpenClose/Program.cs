﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

IRoom standardRoom = new StandardRoom(); // đặt phòng loại thường.

IRoom vipRoom = new VIPRoom(); // đặt phòng loại VIP.

var roomManager = new RoomManager(standardRoom);
roomManager.BookARoom();

public class RoomManager
{
    private readonly IRoom _room;

    public RoomManager(IRoom room) // sử dụng DI
    {
        _room = room;
    }

    public void BookARoom()
    {
        _room.BookRoom();
    }
}

public interface IRoom
{
    void BookRoom(); // sử dụng cả đa hình, kế thừa, và trừu tượng
}

public class StandardRoom : IRoom
{
    public void BookRoom()
    {
        // Logic để đặt phòng loại thường
        Console.WriteLine("Đã đặt phòng loại thường.");
    }
}

public class VIPRoom : IRoom
{
    public void BookRoom()
    {
        // Logic để đặt phòng loại VIP
        Console.WriteLine("Đã đặt phòng loại VIP.");
    }
}

// Các lớp khác tương tự cho các loại phòng khác
